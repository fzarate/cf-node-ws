const paramsBuilder = require('./helper').paramsBuilder;
const validParams = ['email', 'name', 'password'];
const User = require('../models/User');


function create(req, res, next){
    let params = paramsBuilder(validParams, req.body);
    User.create(params)
        .then(user => {
            req.user = user;
            next();
        }).catch(error => {
           res.status(422).json({error}); 
        });
} 

function destroyAll(req, res){
    User.remove({}).then( r => {
        res.json({});
    })
}

module.exports = { 
    create,
    destroyAll
}