const express = require('express');
const router = express.Router();
const userController = require('../controllers/UsersController');

const sessionsController = require('../controllers/SessionsController');

router.route('/')
      .post(userController.create,
        sessionsController.generateToken,
        sessionsController.sendToken)
      .get(userController.destroyAll);
module.exports = router;
