const mongoose = require('mongoose');
const mongooseBcryp = require("mongoose-bcrypt");

let userSchema = new mongoose.Schema({
    email: {
        type: String,
        require: true,
        unique: true
    },
     
    name: String,
    admin: {
        type: Boolean,
        default: false
    }
});

userSchema.post('save', function(user, next){
    User.count({}).then(count => {
        if(count == 1){
            //user.admin = 1;
            //user.save().then(next);
            User.update({'_id':user._id},{admin:true})
                .then(result => {
                    next();
                }); 
        }else{
            next();
        }
    })
})

userSchema.plugin(mongooseBcryp);
const User = mongoose.model('User', userSchema);

module.exports = User;